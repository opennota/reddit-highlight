// ==UserScript==
// @name        reddit-highlight
// @namespace   reddit
// @include     /^https?://([a-z]+\.|)reddit\.com//
// @version     10
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_addStyle
// @license     GPLv3
// @homepage    https://github.com/opennota/reddit-highlight
// ==/UserScript==

let reqTime = new Date();

const newCommentColor = '#ADFFB2';
const commentsLinkColor = '#FF4500'; // OrangeRed

function getLastVisited(path) {
    let s = GM_getValue(path);
    if (!s) return;
    return new Date(s);
}

function setLastVisited(path, time) {
    GM_setValue(path, time.toISOString());
}

function observe(selector, callback) {
    let observer = new MutationObserver(mutations => {
        mutations.forEach(mutation => {
            Array.forEach(mutation.addedNodes, callback);
        });
    });
    observer.observe(document.querySelector(selector), {
        childList: true,
        subtree: true
    });
}

function getUser() {
    let userLink = document.querySelector('.user > a[href]');
    return userLink ? userLink.textContent : undefined;
}

function scopedSelect(root, selector) {
    if (scopeSupported) {
        return root.querySelector(':scope > ' + selector);
    }
    if (!root.id) {
        root.id = 'RH' + Math.random().toString().substr(2, 10);
    }
    return document.querySelector('#' + root.id + ' > ' + selector);
}

function getCommentTime(comment) {
    let timeEl = scopedSelect(comment, '.entry > .tagline > time.edited-timestamp');
    if (timeEl) {
        return new Date(timeEl.getAttribute('datetime'));
    } else {
        timeEl = scopedSelect(comment, '.entry > .tagline > time.live-timestamp');
        if (timeEl) {
            return new Date(timeEl.getAttribute('datetime'));
        }
    }
}

function processComments() {
    let user = getUser();

    function highlight(comment) {
        let authorLink = scopedSelect(comment, '.entry > .tagline > a.author');
        if (authorLink && authorLink.textContent === user) return;
        comment.querySelector('.usertext-body').className += ' highlight';
    }

    function conditionalHighlight(comment) {
        if (getCommentTime(comment) > lastVisited) {
            highlight(comment);
        }
    }

    let path = location.pathname.replace(/^\/r\/|\/$/g, '');
    let lastVisited = getLastVisited(path);
    let highlightFunc = lastVisited ? conditionalHighlight : highlight;
    Array.forEach(document.querySelectorAll('.comment'), highlightFunc);
    setLastVisited(path, reqTime);

    observe('.commentarea', node => {
        if (node.className.includes('comment')) {
            highlightFunc(node);
        }
    });

    GM_addStyle('.highlight { background-color: ' + newCommentColor + ' !important; }');

    let sitetable = document.querySelector('.commentarea > .sitetable');
    let hsb = document.createElement('div');
    hsb.className = 'highlight-since-box';
    hsb.innerHTML = [
        '<span>Highlight comments since (hh:mm ago):</span>',
        '<input type="text" value="01:00">',
        '<button>highlight</button>'
    ].join('');
    sitetable.parentNode.insertBefore(hsb, sitetable);
    let input = hsb.querySelector('input');
    hsb.querySelector('button').addEventListener('click', _ => {
        let [hours, minutes] = input.value.split(':');
        let diff = (parseInt(hours, 10) * 60 + parseInt(minutes, 10)) * 60 * 1000;
        let now = new Date();

        function highlightSince(comment) {
            let authorLink = scopedSelect(comment, '.entry > .tagline > a.author');
            if (authorLink && authorLink.textContent === user) return;

            let body = comment.querySelector('.usertext-body');
            let highlighted = body.className.includes('highlight');
            if (now - getCommentTime(comment) <= diff) {
                if (!highlighted) {
                    body.className += ' highlight';
                }
            } else if (highlighted) {
                body.className = body.className.replace('highlight', '');
            }
        }

        Array.forEach(document.querySelectorAll('.comment'), highlightSince);

    }, false);

    GM_addStyle([
        '.highlight-since-box {',
        '  color: #583800;',
        '  background-color: #FFFDCC;',
        '  max-width: 500px;',
        '  border: 1px solid #E1B000;',
        '  font-weight: bold;',
        '  padding: 7px 10px 7px 7px;',
        '  display: inline-block;',
        '  margin-left: 10px;',
        '  font-size: 12px;',
        '}',
        '.highlight-since-box input {',
        '  text-align: right;',
        '  margin: auto 5px;',
        '  width: 50px;',
        '  height: 20px;',
        '}'
    ].join(''));
}

function processLinks() {
    let re = new RegExp('^https?://([a-z]+\\.|)reddit\\.com/r/|/$', 'g');

    function conditionalColorLink(link) {
        let path = link.href.replace(re, '');
        if (!getLastVisited(path)) {
            link.className += ' unvisited';
        }
    }

    function colorLinks(root) {
        Array.forEach(root.querySelectorAll('a.comments'), conditionalColorLink);
    }

    colorLinks(document);

    observe('#siteTable', node => {
        if (node.tagName === 'A' && node.className.includes('comments')) {
            conditionalColorLink(node);
        }
        colorLinks(node);
    });

    GM_addStyle('.unvisited { color: ' + commentsLinkColor + ' !important; }');
}

if (/Ow! -- reddit.com/.test(document.querySelector('title').textContent)) // 50x
    return;

// Check if :scope pseudo class is supported by the browser.
var scopeSupported = true;
try {
    document.querySelector(':scope');
} catch (e) {
    scopeSupported = false;
}

if (/^\/r\/[^/]+\/comments\//.test(location.pathname)) {
    processComments();
} else {
    processLinks();
}
